# Sample python code

Code made as exercise to improve my python skills

## Singleton implementation

A class which can instanciate only once

run ```python src/singleton.py```

## Lock implementation

Blocking, non blocking or blocking lock with a timeout

run ```python src/locks_demo.py``` to see it in action in non blocking, blocking mode and with a timeout

### Blocking

```mylock = Lock(max_locks=5, blocking=True)```

A blocking lock will wait until a lock is avaible in pool to return from acquire()

### Blocking with timeout

```mylock = Lock(max_locks=5, timeout=.2)```

If no lock avaible in the next *timeout* seconds, returns False.

### Non blocking

A non blocking lock will return immiediatly True from acquire if lock is acquired or False if no lock is avaible in pool at query time:

```
mylock = Lock(max_locks=2, blocking=False) # 2 locks avaible
mylock.acquire() # True : get a lock. 1 lock avaible
mylock.acquire() # True : get a lock. 0 lock avaible
mylock.acquire() # False : can’t get a lock no more lock avaible
mylock.release() # 1 lock avaible
mylock.acquire() # True : 0 lock avaible
```

### Context manager

```
with Lock(max_locks=1, timeout=0.40) as mylock:
    run_threads(mylock, 10)
```

### Please note:

```
mylock = Lock()
```
is the same as:
```
mylock = Lock(max_locks=1, blocking=True, timeout=-1)
```

```mylock = Lock(max_locks=5, blocking=False, timeout=0.2)```
This Lock is non blocking. blocking option has priority over timeout.

```mylock = Lock(max_locks=5, blocking=True, timeout=-2)```
Blocking lock with no timeout because of the negative value for timeout.

```mylock = Lock(max_locks=5, blocking=True, timeout=0)```
Non blocking lock because of the null timeout

## To do
