"""Test Lock class"""

import random
import threading
from time import sleep

from lock import Lock


def run_threads(fun_lock, number):
    """Run threads"""
    print(f"\n{my_lock}")
    for fun_id in range(number):
        thread = threading.Thread(target=bidule, args=(fun_lock, fun_id))
        thread.start()
        sleep(0.1)
    sleep(0.4)


def bidule(func_lock: Lock, number: int):
    """Do the bidule"""
    bidule_number = f"Bidule({number})"
    print(f"{bidule_number} requesting for a lock")
    if func_lock.acquire():
        print(
            f"{bidule_number} acquired a lock. "
            f"Actives locks: {func_lock.count}/{func_lock.max_locks}"
        )
        sleep(random.random() * 0.6)
        func_lock.release()
        print(
            f"{bidule_number} released a lock. "
            f"Actives locks: {func_lock.count}/{func_lock.max_locks}"
        )
    else:
        # Non blocking or timeout
        print(f"{bidule_number} aborting... No lock avaible")


if __name__ == "__main__":
    non_blk_lock = Lock(max_locks=2, blocking=False)
    blk_lock = Lock(max_locks=2, blocking=True)
    tm_lock = Lock(max_locks=1, timeout=0.40)

    for my_lock in [
        non_blk_lock,
        blk_lock,
        tm_lock,
    ]:

        run_threads(my_lock, 5)

    print("\nAs context manager")
    with Lock(max_locks=1, timeout=0.40) as mylock:
        run_threads(mylock, 5)
