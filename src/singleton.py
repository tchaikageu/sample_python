""" singleton"""


class Singleton:
    """Singleton class"""

    _instance = None

    def __new__(cls, *args, **kargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kargs)
        return cls._instance

    def get_instance(self):
        """Return singleton instance"""
        return self


if __name__ == "__main__":
    mysingleton = Singleton()
    print(f"{mysingleton=}")
    print(f"{mysingleton.get_instance()=}")
    print(f"{Singleton()=}")
    print(f"{Singleton().get_instance()=}")
