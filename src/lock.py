"""Lock implementation"""

import time


class Lock:
    """Lock class settings:
    - max_count: max number of avaible locks
    - blocking: True or False
        True: acquire() is a blocking function which wait forever for an avaible lock
        False: return immedaitly False if no lock avaible
        or True if a lock is acquired
    - timeout: in secs. timeout for getting a lock
    """

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        pass

    def __init__(self, max_locks: int = 1, blocking: bool = True, timeout=-1):
        self.locked = False
        self.max_locks = max_locks
        self.blocking = blocking
        self.timeout = timeout
        self.count = 0

        if self.max_locks < 1:
            # lock will always be locked !
            raise Exception("max_locks must be >=1")

    def __repr__(self):
        return (
            f"Lock(max_locks={self.max_locks}, "
            f"blocking={self.blocking}, "
            f"timeout={self.timeout})"
        )

    def acquire(self) -> bool:
        """Acquire a lock"""

        if self.locked and not self.blocking:
            return False

        start_time = time.time()

        if self.timeout >= 0:
            stop_time = start_time + self.timeout
            # print(f"{stop_time=}")
        else:
            stop_time = start_time * 2

        if self.blocking:
            while self.locked:
                if time.time() >= stop_time:
                    # print("*** Timeout ***")
                    return False

        self.count += 1
        self.locked = self.count == self.max_locks

        return True

    def release(self) -> bool:
        """Release a lock"""
        if self.count == 0:
            # No lock to release
            raise Exception("No lock to release !")

        self.count -= 1
        self.locked = False
        return True
